#include<stdio.h>
void f(int *, int *,int *);
int main(){
	int a=10,b=20,c=15;
	printf("Truoc khi goi ham, a = %d, b = %d,c = %d\n",a,b,c);
	f(&a,&b,&c);
	printf("Sau khi goi ham, a =%d, b = %d, c = %d",a,b,c);
	return 0;
}
void f(int *pa,int *pb,int *pc){
	int temp1 = *pa;
	int temp2 = *pb;
	*pa = *pc;
	*pb = temp1;
	*pc = temp2;
}
