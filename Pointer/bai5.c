#include<stdio.h>
#include<stdlib.h>
void input(float *,int );
void output(float *, int);
void tong(float *p1,float *p2, float *p3,int size);
int main(){
	float *a, *b;
	a = calloc(10,sizeof(float));
	b = calloc(10,sizeof(float));
	int sa,sb;
	printf("Size cua a = "); scanf("%d",&sa);
	printf("Size cua b = "); scanf("%d",&sb);
	printf("Nhap vao mang a: ");
	input(a,sa);
	printf("Nhap vao mang b: ");
	input(b,sb);
	float *c = calloc(10,sizeof(float));
	int sc;
	if (sb>sa) sc = sb;
	else sc = sa;
	tong(a,b,c,sc);
	output(c,sc);
	free(a);
	free(b);
	free(c);
	return 0;
}
void input(float *p, int size){
	int i;
	for (i=0;i<size;i++){
		scanf("%f",(p+i));
	}
}
void output(float *p,int size){
	int i;
	for (i=0;i<size;i++){
		printf("%f",*(p+i));
	}
}
void tong(float *p1, float *p2, float *p3,int size){
	int i;
	for (i=0;i<size;i++){
		*(p3+i) = *(p2+i) + *(p1+i);
	}
}
