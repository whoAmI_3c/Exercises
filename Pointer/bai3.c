#include<stdio.h>
#include<stdlib.h>
void input(int *p,int size);
int countEven(int *,int );
int main(){
	int *p;
	int n;
	printf("Nhap n = ");
	scanf("%d",&n);
	p = calloc(n,sizeof(int));
	input(p,n);
	printf("so phan tu chan: %d",countEven(p,n));
	free(p);
	return 0;
}
int countEven(int *p,int size){
	int i,cnt=0;
	for (i=0;i<size;i++){
		if (*(p+i)%2==0) cnt++;
	}
	return cnt;
}
void input(int *p,int size){
	int i;
	for (i=0;i<size;i++){
		scanf("%d",(p+i));
	}
}
