#include<stdio.h>
#include<math.h>
void input(int a[],int );
float average(int a[],int);
float standard_deviation(int a[],int );
int main(){
	int n;
	int a[n];
	printf("Mang co bn gia tri: ");
	scanf("%d",&n);
	input(a,n);
	printf("ky vong cua day so: %.2f\n",average(a,n));
	printf("Do lech chuan cua day so: %.2f\n",standard_deviation(a,n));
	return 0;
}
void input(int a[],int n){
	int i;
	for (i=0;i<n;i++){
		printf("Nhap gia tri cho a[%d] = ",i);
		scanf("%d",&a[i]);
	}
}
float average(int a[],int n){
	float s = 0;
	int i;
	for (i=0;i<n;i++){
		s += a[i];
	}
	return (float)s/n;
}
float standard_deviation(int a[],int n){
	float tb = average(a,n);
	int i=0;
	float s;
	for (i=0;i<n;i++){
		s+=pow((tb-a[i]),2);
	}
	return sqrt((s/(n)));
}
