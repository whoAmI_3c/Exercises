#include<stdio.h>
#include<stdlib.h>
#include<string.h>
void fix1(char *,int );
void fix2(char *,int );
int main(){
	char *s1=calloc(20,sizeof(char));
	char *s2=calloc(20,sizeof(char));
	gets(s1);
	int n1=strlen(s1);
	fix1(s1,n1);
	fix2(s1,n1);
	puts(s1);
	free(s1);
	free(s2);
	return 0;
}
void fix1(char *s,int size){
	int i,j;
	for (i=0;i<size;i++){
		if (s[i]==' '&&s[i+1]==' '){
			for (j=i;j<size;j++){
				s[j] = s[j+1];
			}
			size--;
			i--;
		}
	}
}
void fix2(char *s,int size){
	int i,j;
	s[0]=toupper(s[0]);
	for (i=1,j=i+1;j<size;i++,j++){
		if (s[i]==' ') s[j]=toupper(s[j]);
		else s[j]=tolower(s[j]);
	}
}
