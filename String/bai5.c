#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
void chuyenThuong(char *, int );
void chuyenHoa(char *, int);
int main(){
	char *s1 = calloc(100,sizeof(char));
	char *s2 = calloc(100,sizeof(char));
	printf("Nhap chuoi muon chuyen sang HOA: ");
	gets(s1);
	int n1= strlen(s1);
	chuyenHoa(s1,n1);
	printf("Nhap chuoi muon chuyen sang thuong: ");
	gets(s2);
	int n2=strlen(s2);
	chuyenThuong(s2,n2);
	printf("Chuoi hoa: %s\n",s1);
	printf("Chuoi thuong: "); puts(s2);
	free(s1);
	free(s2);
	return 0;
}
void chuyenThuong(char *s,int size){
	int i;
	for (i=0;i<size;i++){
		if (s[i]>='A'&&s[i]<='Z') s[i] = tolower(s[i]);
	}
}
void chuyenHoa(char *s,int size){
	int i;
	for (i=0;i<size;i++){
		if (s[i]>='a'&&s[i]<='z') s[i] = toupper(s[i]);
	}
}
