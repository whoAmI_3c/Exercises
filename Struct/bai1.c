#include<stdio.h>
#include<stdlib.h>
typedef struct{
	int mnv;
	char ten[30];
	char diachi[30];
}nv;
void input(nv [], int *);
void output(nv [], int);
int main(){
	nv *list=calloc(10,sizeof(nv));
	int n=0;
	input(list,&n);
	output(list,n);
	return 0;
}
void input(nv list[],int *p){
	int n;
	printf("Nhap vao tong so nv: ");
	scanf("%d",&n);
	*p=n;
	int i;
	for (i=0;i<n;i++){
		printf("Nhap ma nhan vien thu %d\n",i+1);
		printf("Ma nhan vien: ");
		scanf("%d",&list[i].mnv);
		printf("Ten: ");
		fflush(stdin); gets(list[i].ten);
		printf("Dia chi: ");
		fflush(stdin); gets(list[i].diachi);
	}
}
void output(nv list[], int size){
	int i;
	printf("-------------------------------------------------------------\n");
	printf("               DANH SACH NHAN VIEN\n");
	printf("-------------------------------------------------------------\n");
	printf("%-10s%-30s%-30s\n","MA SO","HO TEN","DIA CHI");
	for (i=0;i<size;i++){
		printf("%-10d%-30s%-30s\n",list[i].mnv,list[i].ten,list[i].diachi);
	}
}
