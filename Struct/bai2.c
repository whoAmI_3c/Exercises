#include<stdio.h>
#include<stdlib.h>
typedef struct{
	float toan;
	float ly;
	float hoa;
}DiemThi;
typedef struct{
	int ngay;
	int thang;
	int nam;
}NgaySinh;
typedef struct{
	int sbd;
	char hoten[30];
	NgaySinh ngaysinh;
	char truong[30];
	DiemThi diemthi;
}SinhVien;
void sinh(int *, int *, int *);
void input(SinhVien [], int *);
void diem(float *, float *, float *);
void output(SinhVien [], int );
int main(){
	int n=0;
	SinhVien list[10];
	input(list,&n);
	output(list,n);
	return 0;
}
void input(SinhVien list[], int *p){
	int n=0;
	printf("Nhap vao tong so sinh vien: ");
	scanf("%d",&n);
	*p=n;
	int i;
	for (i=0;i<n;i++){
		printf("Nhap sinh vien thu %d\n",i+1);
		printf("Ho ten: ");
		fflush(stdin); gets(list[i].hoten);
		fflush(stdin); sinh(&list[i].ngaysinh.ngay,&list[i].ngaysinh.thang,&list[i].ngaysinh.nam);
		printf("Truong: ");
		fflush(stdin); gets(list[i].truong);
		printf("So bao danh: ");
		fflush(stdin); scanf("%d",&list[i].sbd);
		fflush(stdin); diem(&list[i].diemthi.toan,&list[i].diemthi.ly,&list[i].diemthi.hoa);
		printf("\n");
	}
}
void output(SinhVien list[], int size){
	int i;
	printf("-------------------------------------------------------------\n");
	printf("               DANH SACH NHAN VIEN\n");
	printf("-------------------------------------------------------------\n");
	printf("%-30s%-17s%-19s%-16s%-17s\n","    HO TEN","NGAY SINH","TRUONG","SOBAODANH","DIEM");
	for (i=0;i<size;i++){
		if(list[i].diemthi.toan+list[i].diemthi.ly+list[i].diemthi.hoa>=15){
		printf("%-30s%",list[i].hoten);
		printf("%.2d/%.2d/%-12.4d",list[i].ngaysinh.ngay, list[i].ngaysinh.thang,list[i].ngaysinh.nam);
		printf("%-20s%-15d",list[i].truong,list[i].sbd);
		printf("%g\n",list[i].diemthi.toan+list[i].diemthi.ly+list[i].diemthi.hoa);
		}
	}
}
void sinh(int *x, int *y, int *z){
	printf("Ngay sinh: ");
	scanf("%d/%d/%d",x,y,z);
}
void diem(float *x, float *y, float *z){
	printf("Diem Toan: ");
	scanf("%f",x);
	printf("Diem Ly: ");
	scanf("%f",y);
	printf("Diem Hoa: ");
	scanf("%f",z);
}

